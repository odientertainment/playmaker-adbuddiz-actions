﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("ODI AdBuddiz")]
	[Tooltip("Inicializa a API do AdBuddiz. Deve ser chamada na inicializaçao do app.")]
	public class InitAdBuddiz : FsmStateAction
	{
		public bool enableAndroid;
		public string androidPublisherKey = "TEST_PUBLISHER_KEY_ANDROID";
		public bool enableIOS;
		public string iOSPublisherKey = "TEST_PUBLISHER_KEY_IOS";
		public bool testMode;
		public bool showLogs;

		public override void OnEnter()
		{
			if(testMode){
				AdBuddizBinding.SetTestModeActive();
			}
			
			if(showLogs){
				AdBuddizBinding.SetLogLevel(AdBuddizBinding.ABLogLevel.Error); 
			}

			if(enableAndroid){
				AdBuddizBinding.SetAndroidPublisherKey(androidPublisherKey);
			}

			if(enableIOS){
				AdBuddizBinding.SetIOSPublisherKey(iOSPublisherKey);
			}

			if(enableAndroid || enableIOS){
				AdBuddizBinding.CacheAds();
			}

			Finish();
		}
	}
}
