﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("ODI AdBuddiz")]
	[Tooltip("Mostra o Ads do AdBuddiz.")]
	public class Show : FsmStateAction
	{
		[Tooltip("Not Ready Event")]
		public FsmEvent OnNotReady;
		
		[Tooltip("Fail Event")]
		public FsmEvent OnFail;

		[Tooltip("Show Event")]
		public FsmEvent OnShow;

		[Tooltip("Click Event")]
		public FsmEvent OnClick;

		[Tooltip("Hide Event")]
		public FsmEvent OnHide;

		public override void OnEnter()
		{
			if (AdBuddizBinding.IsReadyToShowAd()) {
				ShowAd();
			}else{
				Fsm.Event(OnNotReady);
			}
		}

		void ShowAd()
		{
			AdBuddizManager.didFailToShowAd += DidFailToShowAd;
			AdBuddizManager.didShowAd += DidShowAd;
			AdBuddizManager.didClick += DidClick;
			AdBuddizManager.didHideAd += DidHideAd;
			
			AdBuddizBinding.ShowAd();
		}

		// unregister as a listener
		void OnDisable() { 
			AdBuddizManager.didFailToShowAd -= DidFailToShowAd;
			AdBuddizManager.didShowAd -= DidShowAd;
			AdBuddizManager.didClick -= DidClick;
			AdBuddizManager.didHideAd -= DidHideAd;
			Finish();
		}

		void DidFailToShowAd(string adBuddizError) 
		{
			Fsm.Event(OnFail);
			OnDisable();
		}

		void DidShowAd() 
		{
			Fsm.Event(OnShow);
		}

		void DidClick() 
		{
			Fsm.Event(OnClick);
		}

		void DidHideAd() 
		{
			Fsm.Event(OnHide);
			OnDisable();
		}
	}
}
